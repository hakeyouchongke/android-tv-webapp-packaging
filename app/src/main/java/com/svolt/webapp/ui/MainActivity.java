package com.svolt.webapp.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.svolt.webapp.R;

import org.mozilla.geckoview.GeckoRuntime;
import org.mozilla.geckoview.GeckoSession;
import org.mozilla.geckoview.GeckoView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

public class MainActivity extends FragmentActivity {

    private GeckoView mGeckoView;
    //  private String mUrl = "https://svolt.dev.juyuansoft.com/andon/index_pc.html";
//  private String mUrl = "http://10.36.30.129:8081/andon/index_pc.html";
//  private String mUrl = "http://10.36.23.95:32700/andon/index_pc.html";
//  private String mUrl = "https://svolt.dev.juyuansoft.com/andon/electronic.html";
//  private String mUrl = "http://49.232.158.75:8081/andon/electronic.html";
//  private String mUrl = "http://10.36.23.95:31150/dashboard/PC/electronic.html";
//  private String mUrl = "http://10.36.23.95:31150/dashboard/PC/parkChoose.html";
//  private String mUrl = "http://10.36.23.95:31150/dashboard/PC/parkChoose.html";
    private String mUrl = "http://10.36.30.84:8100/wcb";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); // 隐藏标题
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置全屏
        setContentView(R.layout.activity_main);
        GeckoSession session = new GeckoSession();
        session.open(GeckoRuntime.getDefault(this));
        mGeckoView = findViewById(R.id.geckoview);
        mGeckoView.setSession(session);

        session.loadUri(mUrl); // Or any other URL...
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
